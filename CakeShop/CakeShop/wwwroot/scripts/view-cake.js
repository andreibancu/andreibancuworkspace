﻿const cakeControllerUri = "/api/cake/";
const reviewControllerUri = "/api/review/";

$(document).ready(function () {
    var cakeId = getUrlVars()["cakeid"];
    getCake(cakeId);
    getReviews();
   
    
});

// Read a page's GET URL variables and return them as an associative array.
function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function displayCakeDetails(cake) {
    $('#cake-name').html(cake.name);
    $('#cake-price').html(cake.price + ' lei');
    $('#cake-description').html(cake.description);
    $('#cake-category').html(cake.category);
    $('#cake-image').css('max-height', '400px');
    $('#cake-image').attr("src", cake.base64Image);
}

function getCake(cakeId) {
    $.ajax({
        type: "GET",
        url: cakeControllerUri + cakeId,
        cache: false,
        success: function (data) {
            displayCakeDetails(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert("Something went wrong!");
        }
    });
}


function getReviews() {
    $.ajax({
        type: "GET",
        url: reviewControllerUri,
        cache: false,
        success: function (data) {
            displayReviews(data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert("Something went wrong!");
        }
    });
}

function closeInput() {
    $("#spoiler").css({ display: "none" });
}

function formAppear() {
    $("#spoiler").css({ display: "block" });
}

function displayReviews(cakes) {
    $("#review-div").empty();

    var cakeId = getUrlVars()["cakeid"];

    $.each(cakes, function (key, review) {
        if (review.cakeId == cakeId) {
            $("#review-div").append(' <p id="review-p">' + review.comment + ' </p > <small id="review-small" class="text-muted">Posted by  ' + review.email + ' on ' + review.dateReview + ' </small > <hr>');

        }
    });
    
}

function addReview() {
    var cakeId = getUrlVars()["cakeid"];

    const review = {
        email: $("#email").val(),
        comment: $("#comment").val(),
        cakeId: cakeId,
        
    };
    $.ajax({
        type: "POST",
        accepts: "application/json",
        url: reviewControllerUri,
        contentType: "application/json",
        data: JSON.stringify(review),
        error: function (jqXHR, textStatus, errorThrown) {
            alert("Something went wrong!");
        },
        success: function (result) {
            getReviews();
            $("#email").val("");
            $("#comment").val("");
            
        }
    });

    closeInput();
    return false;
}

$(".add-review-form").on("submit", function () {
    addReview();

    return false;
});
